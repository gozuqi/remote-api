require 'hudson-remote-cli'
require 'dotenv'

Dotenv.load


Hudson[:url] = ENV['URL']
Hudson[:user] = ENV['USERS']
Hudson[:password] = ENV['PASSWORD']


## list jobs 
p "list jobs"
Hudson.jobs.each { |j|
  p j
}

## create client
p "create client"
p j = Hudson::Job.new('suzuki-test-build')

## build client
p j.build({'OSHIROBRANCH' => 'master', 'VERTXBRANCH' => 'develop'})

sleep 1

## returns true if job is currently running
t = Hudson::Job.new('test_std_job')
if t.active? == true
  p "job active true"
else
  p "job compile"
end


 
